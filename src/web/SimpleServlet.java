package web;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet("/SimpleServlet")
public class SimpleServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
   
    public SimpleServlet() {
        super();
    }

	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		PrintWriter out = response.getWriter();
		String name = (String) request.getParameter("name");
		
		HttpSession session = request.getSession();
		ServletContext ctx = getServletContext();
		if(name!=null && !name.equals("")){
			session.setAttribute("name", name);
			ctx.setAttribute("name", name);
		}
		out.println("request: "+name+ "session:"+ session.getAttribute("name")+ 
				"ctx: "+ ctx.getAttribute("name"));
		
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException { 
		PrintWriter out = response.getWriter(); 
		String login = (String) request.getParameter("login"); 
		ServletContext ctx = getServletContext(); 
		if(ctx.getAttribute(login) == null){ 
			ctx.setAttribute(login, 0); 
		} 
		int temp = (Integer) ctx.getAttribute(login); 
		temp++; 
		ctx.setAttribute(login, temp); 
		out.println("Witaj "+login+"! To Twoje "+temp+" logowanie."); 
	}

}
